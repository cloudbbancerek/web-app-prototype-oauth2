package ct.configuration.jwt;

import ct.api.user.UserHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

@Configuration
public class JwtConfiguration {

    @Value("${jwt.signing-key:KEY}")
    private String signingKey;

    @Value("${jwt.algorithm:HMACSHA256}")
    private String algorithm;

    @Autowired
    private UserHelper userHelper;

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter tokenConverter = new CustomJwtAccessTokenConverter(userHelper);

        tokenConverter.setSigningKey(signingKey);
        SecretKey secretKey = new SecretKeySpec(signingKey.getBytes(), algorithm);
        tokenConverter.setSigner(new MacSigner(algorithm, secretKey));
        tokenConverter.setVerifier(new MacSigner(algorithm, secretKey));

        DefaultAccessTokenConverter accessTokenConverter = new DefaultAccessTokenConverter();
        accessTokenConverter.setUserTokenConverter(new CustomUserAuthenticationConverter(userHelper));
        tokenConverter.setAccessTokenConverter(accessTokenConverter);

        return tokenConverter;
    }

    @Bean
    public JwtTokenStore jwtTokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

}
