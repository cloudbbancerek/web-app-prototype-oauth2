package ct.configuration.jwt;

import ct.api.user.UserHelper;
import ct.api.user.model.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.LinkedHashMap;
import java.util.Map;

public class CustomJwtAccessTokenConverter extends JwtAccessTokenConverter {

    private UserHelper userHelper;

    public CustomJwtAccessTokenConverter(UserHelper userHelper) {
        this.userHelper = userHelper;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        User user = (User) authentication.getPrincipal();

        Map<String, Object> additionalInfo = new LinkedHashMap<>(accessToken.getAdditionalInformation());
        additionalInfo.put("user", userHelper.serializeUser(user));

        DefaultOAuth2AccessToken customAccessToken = new DefaultOAuth2AccessToken(accessToken);
        customAccessToken.setAdditionalInformation(additionalInfo);

        return super.enhance(customAccessToken, authentication);
    }
}
