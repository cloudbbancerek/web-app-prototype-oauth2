package ct.configuration.jwt;

import ct.api.user.UserHelper;
import ct.api.user.model.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;

import java.util.Map;

public class CustomUserAuthenticationConverter extends DefaultUserAuthenticationConverter {

    private UserHelper userHelper;

    public CustomUserAuthenticationConverter(UserHelper userHelper) {
        this.userHelper = userHelper;
    }

    private boolean isValidJwtToken(Map<String, ?> map) {
        return map.get("user") != null;
    }

    private Map<String, Object> getSerializedUser(Map<String, ?> map) {
        try {
            return (Map<String, Object>) map.get("user");
        } catch (ClassCastException e) {
            return null;
        }
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (!isValidJwtToken(map)) {
            return null;
        }

        Map<String, Object> serializedUser = getSerializedUser(map);
        if (serializedUser == null) {
            return null;
        }

        User user = userHelper.deserializeUser(serializedUser);

        return new UsernamePasswordAuthenticationToken(user, "N/A", user.getAuthorities());
    }
}
