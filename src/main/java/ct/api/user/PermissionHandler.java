package ct.api.user;

import ct.api.user.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PermissionHandler {

    private User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        return user;
    }

    private boolean hasRole(User user, String role) {
        return user.getRoles().stream()
                .anyMatch(r -> Objects.equals(r.getName(), role));
    }

    public boolean hasRole(String role) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        return hasRole(user, role);
    }

    public boolean hasAnyRole(String... roles) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        for (String role : roles) {
            if (hasRole(user, role)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAllRoles(String... roles) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        for (String role : roles) {
            if (!hasRole(user, role)) {
                return false;
            }
        }
        return true;
    }

    private boolean hasPrivilege(User user, String privilege) {
        return user.getAuthorities().stream()
                .anyMatch(p -> Objects.equals(((GrantedAuthority) p).getAuthority(), privilege));
    }

    public boolean hasPrivilege(String privilege) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        return hasPrivilege(user, privilege);
    }

    public boolean hasAnyPrivilege(String... privileges) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        for (String privilege : privileges) {
            if (hasPrivilege(user, privilege)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAllPrivileges(String... privileges) {
        User user = getCurrentUser();
        if (user == null) {
            return false;
        }
        for (String privilege : privileges) {
            if (!hasPrivilege(user, privilege)) {
                return false;
            }
        }
        return true;
    }
}
