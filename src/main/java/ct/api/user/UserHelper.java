package ct.api.user;

import ct.api.user.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class UserHelper {

    private ObjectMapper objectMapper = new ObjectMapper();

    public Map<String, Object> serializeUser(User user) {
        Map<String, Object> serializedUser = objectMapper.convertValue(user, Map.class);
        return serializedUser;
    }

    public User deserializeUser(Map<String, Object> serializedUser) {
        User user = objectMapper.convertValue(serializedUser, User.class);
        return user;
    }
}
