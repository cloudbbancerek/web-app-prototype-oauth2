# What to do first?
* Configure database (src/main/resources/application.properties)
* Run SQL script creating OAuth2 tables (src/main/resources/scripts/oauth2schema.sql)
* Create OAuth2 server clients - this can be done uncommenting block of code in configure method in OAuth2AuthorizationServerConfiguration class.
* Create user structure in database - "spring.jpa.hibernate.ddl-auto" option set to "update" (src/main/resources/application.properties).
* Create actual users to work with - alter init method in InitUsers class and trigger it in Application's main method (src/main/java/ct/Application.java).
* Run the Application (src/main/java/ct/Application.java)

# How to use/test it?
* Hit the 'http://localhost:8080/oauth/token' endpoint:
    * curl -X POST --user client:secret -d 'grant_type=password&username=[username]&password=[password]&resourceId=[resourceId]' http://localhost:8080/oauth/token
* Server responds with JSON containing access_token.
* Hit the 'http://localhost:8080/test/me' endpoint with token passed via 'Authorization' header:
    * curl -X GET -H 'Authorization: Bearer [access_token]' http://localhost:8080/test/me
* You may get 403 code because of @PreAuthorize annotation on method associated with this endpoint.



# src/main/resources/application.properties
* database configuration
* spring.jpa.hibernate.ddl-auto - creates table structure when set to "update" (it's better to switch it to "validate")
* jwt.signing-key - key signing JWT
* jwt.algorithm - signing algorithm (HMACSHA512)
* resource-server.resource-id - resource server (API) name, this name has to be included in client's JWT for client to get access to resource
* web-security.debug
* cors.enable-filter - filter, which responds with 200 code when HTTP OPTIONS request is sent - browsers may send OPTIONS request before sending an actual request


# src/main/java/ct/configuration

* MethodSecurityConfiguration.java:
    * allows @PreAuthorize annotations on methods

* OAuth2AuthorizationServerConfiguration.java:
    * sets OAuth2 Server clients' database
    * sets PasswordEncoder
    * sets TokenStore
    * etc.

* OAuth2ResourceServerConfiguration.java:
    * configuration of a resource server (API)
    * configuration of HTTP security

* WebSecurityConfiguration.java:
    * defines some basic beans such as PasswordEncoder

* CustomCorsFilter.java:
    * responds with 200 code when HTTP OPTIONS request is sent


#  User model
User has a collection of roles.
Role consists of a collection of privileges.


# Workarounds
CustomCorsFilter.java - it is supposed to handle situation when a browser sends OPTIONS request before an actual request. This filters responds with 200 code for such requests.
However, it may causes server to throw bunch of exceptions.
